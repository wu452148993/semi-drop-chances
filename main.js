export function setup(ctx) {
    const id = 'drop-chances';
    const title = 'Show % Drop Chances';
    const desc =
        'This script injects into the drop chances menu and displays the % chance of receiving any item in the table. Also shows average GP values';


    const dropChances = () => {
        window.viewMonsterDrops = (monster) => {
            if (monster === undefined) {
                return
            }

            if (monster.lootTable !== undefined) {
                let drops = '';
                let dropsOrdered = [];
                let totalWeight = monster.lootTable.totalWeight
                if (monster.lootChance != undefined) {
                    totalWeight = (totalWeight * 100) / monster.lootChance;
                }
                for (let i = 0; i < monster.lootTable.drops.length; i++) {
                    let dropItem = monster.lootTable.drops[i]
                    dropsOrdered.push({
                        item: dropItem.item,
                        w: dropItem.weight,
                        qty: dropItem.maxQuantity,
                        chance: ` (${((dropItem.weight / totalWeight) * 100).toPrecision(3)}%)`,
                    });
                }
                dropsOrdered.sort(function (a, b) {
                    return b.w - a.w;
                });
                for (let i = 0; i < dropsOrdered.length; i++) {
                    drops +=
                        'Up to ' +
                        dropsOrdered[i].qty +
                        ' x <img class="skill-icon-xs mr-2" src="' +
                        dropsOrdered[i].item.media +
                        '">' +
                        dropsOrdered[i].item.name +
                        dropsOrdered[i].chance +
                        '<br>';
                }
                let bones = '';
                if (monster.bones)
                    bones =
                        'Always Drops:<br><small><img class="skill-icon-xs mr-2" src="' +
                        monster.bones.item.media +
                        '">' +
                        monster.bones.item.name +
                        '</small><br><br>';
                Swal.fire({
                    title: monster.name,
                    html:
                        bones + 'Possible Extra Drops:<br><small>In order of most common to least common<br>' + drops,
                    imageUrl: monster.media,
                    imageWidth: 64,
                    imageHeight: 64,
                    imageAlt: monster.name,
                });
            }
        };

        window.viewItemContents = (item) => {
            if (!item) {
                item = game.bank.selectedBankItem.item;
            } 
            let drops = '';
            let dropsOrdered = [];
            let totalWeight = item.dropTable.totalWeight
            for (let i = 0; i < item.dropTable.drops.length; i++) {
                let dropItem = item.dropTable.drops[i]
                dropsOrdered.push({
                    item: dropItem?.item,
                    w: dropItem?.weight,
                    qty: dropItem?.maxQuantity,
                    id: i,
                    chance: ` (${((dropItem?.weight / totalWeight) * 100).toPrecision(3)}%)`,
                }); 
            }

            dropsOrdered.sort(function (a, b) {
                return b.w - a.w;
            });

            for (let i = 0; i < dropsOrdered.length; i++) {
                drops +=
                    'Up to ' +
                    numberWithCommas(dropsOrdered[i].qty) +
                    ' x <img class="skill-icon-xs mr-2" src="' +
                    dropsOrdered[i].item.media +
                    '">' +
                    dropsOrdered[i].item.name +
                    dropsOrdered[i].chance +
                    '<br>';
            }
            Swal.fire({
                title: item.name,
                html: 'Possible Items:<br><small>' + drops,
                imageUrl: item.media,
                imageWidth: 64,
                imageHeight: 64,
                imageAlt: item.name,
            });
        };

        thievingMenu.showNPCDrops = (npc, area) => {
            const sortedTable = npc.lootTable.drops.sort((itemA, itemB) => itemB.weight - itemA.weight)
            const { minGP, maxGP } = game.thieving.getNPCGPRange(npc);
            let html = `<small><img class="skill-icon-xs mr-2" src="${cdnMedia(
                'assets/media/main/coins.svg'
            )}"> ${formatNumber(minGP)}-${formatNumber(maxGP)} GP</small><br>`;
            html += 'Possible Common Drops:<br><small>';
            if (sortedTable.length) {
                html += `In order of most to least common<br>`;

                const totalWeight = npc.lootTable.totalWeight
                html += npc.lootTable.drops.map(drop => {
                    let text = `${
                        drop.maxQuantity > 1 ? '1-' : ''
                    }${drop.maxQuantity} x <img class="skill-icon-xs mr-2" src="${drop.item.media}">${drop.item.name}`;
                    text += ` (${((100 * drop.weight) / totalWeight).toPrecision(3)}%)`;
                    return text;
                })
                .join('<br>');
                html += `<br>Average Value: ${npc.lootTable.averageDropValue.toPrecision(3)} GP<br>`;
            } else {
                html += 'This NPC doesn&apos;t have any Common Drops';
            }
            html += `</small><br>`;
            html += `Possible Rare Drops:<br><small>`;
            const generalRareHtml = []
            game.thieving.generalRareItems.forEach(({item, npcs, chance})=>{
                    if (npcs === undefined || npcs.has(npc))
                        generalRareHtml.push(`${thievingMenu.formatSpecialDrop(item)} (${(game.thieving.areaUniqueChance * chance).toPrecision(3)}%)`);
            });
            html += generalRareHtml.join('<br>');
            html += `</small><br>`;
            if (area.uniqueDrops.length) {
                html += `Possible Area Unique Drops:<br><small>`;
                html += area.uniqueDrops
                    .map((uniqueRareItem) => {
                        let chance = game.thieving.isPoolTierActive(3) ? 3 : 1;
                        let text = `${thievingMenu.formatSpecialDrop(uniqueRareItem.item, uniqueRareItem.quantity)} (${(game.thieving.areaUniqueChance * chance).toPrecision(3)}%)`
                        return text;
                    })
                    .join('<br>');
                html += '</small><br>';
            }
            if (npc.uniqueDrop) {
                html += `Possible NPC Unique Drop:<br><small>${thievingMenu.formatSpecialDrop(
                    npc.uniqueDrop.item,
                    npc.uniqueDrop.quantity
                )} (${(game.thieving.getNPCPickpocket(npc)).toPrecision(3)}%)</small>`;
            }
            Swal.fire({
                title: npc.name,
                html,
                imageUrl: npc.media,
                imageWidth: 64,
                imageHeight: 64,
                imageAlt: npc.name,
            });
        };

        mod.api.SEMI.log(id, 'Mod loaded successfully')
    };

    ctx.onInterfaceReady(() => {
        dropChances()
    })
}
